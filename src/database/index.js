const mongoose = require('mongoose');
const { dbConfig } = require('../../settings/config');

mongoose.promise = Promise;

const connString = dbConfig.cnx_string
  || `mongodb://${dbConfig.username}:${dbConfig.password}@${dbConfig.host}:${dbConfig.port}/${
    dbConfig.name
  }?authSource=admin`;

const instance = mongoose.connect(connString, { useNewUrlParser: true });

module.exports = instance;
