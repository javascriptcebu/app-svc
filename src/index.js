const debug = require('debug')('app:');
const debugError = require('debug')('app:error:');
const app = require('./app');
const databaseInstance = require('./database');
const { port, host } = require('../settings/config');

databaseInstance
  .then(() => {
    debug('database connected!');

    app.listen(port, () => {
      debug(`Listening on ${host}:${port}`);
    });
  })
  .catch((e) => {
    debugError('%O', e);
  });
