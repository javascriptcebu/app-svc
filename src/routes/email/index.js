const express = require('express');
const router = express.Router();
const controller = require('./controller');
const { checkJwt, jwtAuthz } = require('../../../settings/security');

router.get('/', (req, res) => res.send('email route'));

router.post(
  '/send',
  checkJwt,
  jwtAuthz(['send:email'], {
    checkAllScopes: true,
    customScopeKey: 'permissions'
  }),
  async (req, res) => {
    const data = req.body;

    if (data.is_multiple) {
      const response = await controller.send(
        data.subject,
        data.receiver,
        data.b64_template,
        data.template_vars,
        data.attachments
      );
      if (response.length !== 0) {
        if (response[0].statusCode === 202) {
          res.status(200);
          res.send({ status: true, message: 'Message sent' });
        } else {
          res.status(502);
          res.send({ status: false, message: 'Message not sent' });
        }
      } else {
        res.status(502);
        res.send({ status: false, message: response });
      }
    } else {
      const response = await controller.send_multiple(
        data.subject,
        data.receiver,
        data.b64_template,
        data.template_vars,
        data.attachments
      );
      if (response.length !== 0) {
        if (response[0].statusCode === 202) {
          res.status(200);
          res.send({ status: true, message: 'Message sent' });
        } else {
          res.status(502);
          res.send({ status: false, message: 'Message not sent' });
        }
      } else {
        res.status(502);
        res.send({ status: false, message: response });
      }
    }
  }
);

module.exports = router;
