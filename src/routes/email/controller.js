const sgMail = require('@sendgrid/mail');
const { mailerConfig } = require('../../../settings/config');
const ejs = require('ejs');

/* eslint new-cap: 0 */

sgMail.setApiKey(mailerConfig.key);

async function send(subject, receiverAddress, b64_template, template_var, b64_attachments) {
  try {
    const template = new Buffer.from(b64_template, 'base64').toString('utf-8');
    const rendered = await ejs.render(template, template_var, { async: true });

    const mailOptions = {
      from: mailerConfig.admin,
      to: receiverAddress,
      subject,
      html: rendered,
      attachments: b64_attachments,
    };

    const response = await sgMail.send(mailOptions);

    return response;
  } catch (err) {
    return err;
  }
}

async function send_multiple(subject, receiverAddresses, b64_template, template_var, b64_attachments) {
  try {
    const template = new Buffer.from(b64_template, 'base64').toString('utf-8');
    const rendered = await ejs.render(template, template_var, { async: true });

    const mailOptions = {
      from: mailerConfig.admin,
      to: receiverAddresses,
      subject,
      html: rendered,
      attachments: b64_attachments,
    };

    const response = await sgMail.sendMultiple(mailOptions);

    return response;
  } catch (err) {
    return err;
  }
}

module.exports = { send, send_multiple };
