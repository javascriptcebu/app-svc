const logger = require('debug')('error');
const { User } = require('../../models');

module.exports = async (req, res) => {
  try {
    // isDeleted record are excluded
    const result = await User.findAll();

    return res.json({
      result,
    });
  } catch (e) {
    const { code, severity, routine } = e.original;
    logger('%O', e.original);

    return res.json({
      code,
      severity,
      message: routine,
      error: true,
    });
  }
};
