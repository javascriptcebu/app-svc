const { User } = require('../../models');

module.exports = async (req, res) => {
  const { params } = req;
  const { id } = params;

  if (!id) {
    return res.json({
      error: true,
      message: 'specify id in parameter',
    });
  }

  try {
    const { body } = req;
    const newBody = Object.assign(
      {},
      body,
    );

    await User.update(newBody, {
      where: {
        id,
      },
    });

    return res.json({
      message: 'user updated',
    });
  } catch (e) {
    return res.json({
      error: true,
      message: e.message,
    });
  }
};
