const { User } = require('../../models');

module.exports = async (req, res) => {
  const { params } = req;
  const { id } = params;

  if (!id) {
    return res.json({
      error: true,
      message: 'specify id in parameter',
    });
  }

  try {
    await User.destroy({
      where: {
        id,
      },
    });

    return res.json({
      message: 'user deleted',
    });
  } catch (e) {
    return res.json({
      error: true,
      message: e.message,
    });
  }
};
