const log = require('debug')('create');
const logError = require('debug')('error');
const { User } = require('../../models');

module.exports = async (req, res) => {
  try {
    const newBody = Object.assign({}, req.body);

    log(' new data :%O', newBody);

    await User.create(newBody);

    return res.json({
      creation: true,
    });
  } catch (e) {
    logError('%O', e);

    return res.json({
      error: true,
      message: e.message,
    });
  }
};
