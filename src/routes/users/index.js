const router = require('express').Router();
const find = require('./find');
const create = require('./create');
const patch = require('./patch');
const remove = require('./remove');

router.get('/', find);
router.post('/', create);
router.patch('/:id?', patch);
router.delete('/:id?', remove);

module.exports = router;
