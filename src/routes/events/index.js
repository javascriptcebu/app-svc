const express = require('express');
const router = express.Router();
const { checkJwt, jwtAuthz } = require('../../../settings/security');
const {
  createEvent,
  eventComposer,
  updateEvent,
  getEvent,
  updateParticipant,
  addParticipants
} = require('./controller');

// Create new Event
router.post(
  '/',
  jwtAuthz(['create:event'], {
    checkAllScopes: true,
    customScopeKey: 'permissions'
  }),
  async (req, res) => {
    const resp = await createEvent(eventComposer(req.body));
    if (resp.hasOwnProperty('errors') || resp.hasOwnProperty('errmsg')) {
      return res.status(500).send({ error: resp });
    }

    return res.status(201).send(resp);
  }
);

// Update existing Event
router.patch(
  '/',
  jwtAuthz(['update:event'], {
    checkAllScopes: true,
    customScopeKey: 'permissions'
  }),
  async (req, res) => {
    const resp = await updateEvent(req.body);
    if (resp.hasOwnProperty('errors') || resp.hasOwnProperty('errmsg')) {
      return res.status(500).send({ error: resp });
    }

    return res.status(201).send(resp);
  }
);

//Get Event Details and Participants
// omit_participants: true to omit participant list
// omit_event_details: true to only get the participant list
router.get('/:id', async (req, res) => {
  const resp = await getEvent(req.params.id, req.query);
  if (resp.hasOwnProperty('errors') || resp.hasOwnProperty('errmsg')) {
    return res.status(500).send({ error: resp });
  }

  return res.status(200).send(resp);
});

//Add participants
router.post(
  '/:id',
  checkJwt,
  jwtAuthz(['create:participants'], {
    checkAllScopes: true,
    customScopeKey: 'permissions'
  }),
  async (req, res) => {
    const resp = await addParticipants(req.params.id, req.body);
    if (resp.hasOwnProperty('errors') || resp.hasOwnProperty('errmsg')) {
      return res.status(500).send({ error: resp });
    }

    return res.status(201).send(resp);
  }
);

//Update participants
router.patch(
  '/:id',
  jwtAuthz(['update:participants'], {
    checkAllScopes: true,
    customScopeKey: 'permissions'
  }),
  async (req, res) => {
    const resp = await updateParticipant(req.params.id, req.body);
    if (resp.hasOwnProperty('errors') || resp.hasOwnProperty('errmsg')) {
      return res.status(500).send({ error: resp });
    }

    return res.status(200).send(resp);
  }
);

// router.get('/create', async (req, res) => {
//   const resp = await createEvent(req.body);
//   if (resp.hasOwnProperty('errors')|| resp.hasOwnProperty('errmsg')) {
//     console.log(resp);
//     return res.status(500).send({ error: resp });
//   }

//   return res.status(201).send();
// });

module.exports = router;
