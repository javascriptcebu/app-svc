const { Event, Participant } = require('../../models');

async function queryEvent(attribute) {
  try {
    const query = Event.find(attribute);
    return await query.exec();
  } catch (err) {
    return err;
  }
}

async function queryParticipant(attribute) {
  try {
    const query = Participant.find(attribute);
    return await query.exec();
  } catch (err) {
    return err;
  }
}

async function createEvent(data) {
  const result = await queryEvent({ id: data.id });
  if (result.length !== 0) {
    return { errors: [{ message: 'Event name must be unique' }] };
  }
  try {
    const evt = new Event(data);
    await evt.save();
    return { id: evt.id };
  } catch (err) {
    return err;
  }
}

async function updateEvent(data) {
  const paramData = data;
  const result = await queryEvent({ id: paramData.id });
  if (result.length === 0) {
    return { errors: [{ message: 'Event ID not found' }] };
  }

  try {
    paramData.updatedAt = Date.now();
    const evt = Event.findOneAndUpdate({ id: paramData.id }, paramData);
    return await evt.exec();
  } catch (err) {
    return err;
  }
}

// omit_participants: true to omit participant list
// omit_details: true to only get the participant list
async function getEvent(id, query) {
  const result = await queryEvent({ id });
  if (result.length === 0) {
    return { errors: [{ message: 'Event ID not found' }] };
  }

  if (Object.prototype.hasOwnProperty.call(query, 'omit_participants') && query.omit_participants === 'true') {
    return result[0]._doc;
  }

  const participants = await queryParticipant({ event_id: id });

  if (Object.prototype.hasOwnProperty.call(query, 'omit_details') && query.omit_details === 'true') {
    return participants;
  }

  return { participants, ...result[0]._doc };
}

async function addParticipants(event_id, participantList) {
  const result = await queryEvent({ id: event_id });
  if (result.length === 0) {
    return { errors: [{ message: 'Event ID not found' }] };
  }

  try {
    participantList.forEach(async (lst) => {
      lst.event_id = event_id;
      const prt = new Participant(lst);
      await prt.save();
    });

    return {};
    // const bulk = await Participant.bulkWrite(participantList);
    // console.log(bulk);
  } catch (err) {
    return err;
  }
}

async function updateParticipant(event_id, participantData) {
  const result = await queryParticipant({
    event_id,
    email: participantData.email,
  });

  if (result.length === 0) {
    return { errors: [{ message: 'Participant could not be found' }] };
  }

  try {
    const participant = new Participant(participantData);
    return await participant.update(participantData);
  } catch (err) {
    return err;
  }
}

function eventComposer(data) {
  data.id = `p-${data.name
    .toLowerCase()
    .split(' ')
    .join('-')}`;
  console.log(data);
  return data;
}

module.exports = {
  createEvent,
  eventComposer,
  queryEvent,
  updateEvent,
  addParticipants,
  getEvent,
  updateParticipant,
};
