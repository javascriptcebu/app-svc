const users = require('./users');
const projects = require('./projects');
const peers = require('./peers');
const participants = require('./participants');
const events = require('./events');
const certificates = require('./certificates');
const email = require('./email');

module.exports = app => {
  // register routes
  app.use('/users', users);
  app.use('/projects', projects);
  app.use('/peers', peers);
  app.use('/participants', participants);
  app.use('/events', events);
  app.use('/certificates', certificates);
  app.use('/email', email);
};
