const express = require('express');
const controller = require('./controller');
const { checkJwt, jwtAuthz, getProfile } = require('../../../settings/security');
const router = express.Router();

router.post(
  '/generate/html',
  checkJwt,
  jwtAuthz(['read:users'], {
    checkAllScopes: true,
    customScopeKey: 'permissions',
  }),
  async (req, res) => {
    const data = req.body;
    const profile = await getProfile(req.token);

    const templateConfig = data.template_config;
    templateConfig['values']['name'] = profile['name'].toUpperCase();

    const generated = await controller.generate(data.url, templateConfig);
    if (generated instanceof Error) {
      console.log(generated);
      res.status(500);
      res.send({ success: false, message: generated });
    } else {
      res.status(200);
      res.type('pdf');
      res.end(generated, 'binary');
    }
  },
);

router.post(
  '/generate/pdf',
  checkJwt,
  jwtAuthz(['read:users'], {
    checkAllScopes: true,
    customScopeKey: 'permissions',
  }),
  async (req, res) => {
    data = req.body;
    const profile = await getProfile(req.token);
    console.log(profile);
    data.template_config.forEach(elms => {
      elms['values'].forEach(el => {
        if (el['value'] === 'name') {
          el['value'] = profile['name'];
        }
      });
    });

    generated = await controller.generate_from_pdf(data.url, data.template_config);
    if (generated === null) {
      res.status(500);
      res.send({ success: false, message: 'Error creating certificate' });
    } else {
      res.status(200);
      res.type('pdf');
      res.end(generated, 'binary');
    }
  },
);

module.exports = router;
