const ejs = require('ejs');
const pdf = require('html-pdf');
const HummusRecipe = require('hummus-recipe');
const fetch = require('fetch-base64');

// template_config = {
//     "format": "Letter",
//     "orientation": "landscape",
//     "values": {
//         "name": "Cyrus Zandro Hiyas"
//     }
// }
async function generate(url, template_config) {
  try {
    const b64_template = await fetchUrl(url);
    const template = new Buffer.from(b64_template[0], 'base64').toString('utf-8');
    const { values } = template_config;

    // Deleting values to just get the remaining keys
    delete template_config.values;

    const opts = template_config;
    const rendered = await ejs.render(template, values, { async: true });

    const pdfCreate = new Promise((resolve, reject) => {
      pdf.create(rendered, opts).toBuffer((err, buff) => {
        if (err) {
          reject(err);
        }
        resolve(buff);
      });
    });

    return await pdfCreate;
  } catch (err) {
    return err;
  }
}

// template_config = [{
//     "page": number,
//     "values":
//     [{
//         "value": string,
//         "xPosition": number,
//         "yPosition": number,
//          ... See https://chunyenhuang.github.io/hummusRecipe/Recipe.html#.text for text config
//     }]
// }]
//
async function generate_from_pdf(url, template_config) {
  try {
    const b64_template = await fetchUrl(url);
    const template = new Buffer.from(b64_template[0], 'base64');
    const pdfdoc = new HummusRecipe(template);
    const pdfCreate = new Promise((resolve, reject) => {
      template_config.forEach(vars => {
        template_values = vars.values;
        pdfdoc.editPage(vars.page);
        template_values.forEach(element => {
          const xPos = element.xPosition;
          const yPos = element.yPosition;
          const { value } = element;

          // Deleting extra keys to just get the remaining keys for text config options
          delete element.xPosition;
          delete element.yPosition;
          delete element.value;

          pdfdoc.text(value, xPos, yPos, element);
        });
      });

      pdfdoc.endPage().endPDF(buff => {
        if (buff === null) {
          reject(null);
        }
        resolve(buff);
      });
    });

    return await pdfCreate;
  } catch (err) {
    return err;
  }
}

async function fetchUrl(url) {
  return await new Promise((resolve, reject) => {
    fetch.auto(url).then(
      resp => {
        resolve(resp);
      },
      err => {
        reject(new Error(err));
      },
    );
  });
}

module.exports = { generate, generate_from_pdf };
