const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const compression = require('compression');
const path = require('path');
const bearerToken = require('express-bearer-token');
const routes = require('../routes');
const { logger, errorLogger } = require('../../settings/logging');

const app = express();

app.use(cors());
app.use(compression());
app.use(bearerToken());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));

// Adding Winston as default logger for API Service
app.use(logger);

// static path
const staticPath = path.resolve(__dirname, '../../', 'public');
app.use(express.static(staticPath));

// register routes
routes(app);

// Adding Winston Error logging after Route Init
app.use(errorLogger);

module.exports = app;
