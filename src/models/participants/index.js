const mongoose = require('mongoose');

const { Schema } = mongoose;
const commonFields = require('../common/fields');

const newSchema = new Schema({
  event_id: { type: String, required: true },
  name: { type: String, required: true },
  email: { type: String, required: true },
  hasRegistered: { type: Boolean, default: false },
  hasLoggedIn: { type: Boolean, default: false },
  ...commonFields,
});

module.exports = mongoose.model('participants', newSchema);
