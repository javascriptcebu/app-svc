const User = require('./users');
const Project = require('./projects');
const Peer = require('./peers');
const Certificate = require('./certificates');
const Participant = require('./participants');
const Event = require('./events');

module.exports = {
  User,
  Project,
  Peer,
  Certificate,
  Participant,
  Event,
};
