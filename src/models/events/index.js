const mongoose = require('mongoose');
const commonFields = require('../common/fields');

const { Schema } = mongoose;

const newSchema = new Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  name_space: { type: String, required: true }, // ex. angular, react, vue
  certificate_template: {
    file_format: { type: String }, // either pdf or html
    url: { type: String }, // url where we uploaded the certificate (cloudinary?)
  },
  venue: {
    name: { type: String, default: '' },
    lat: { type: Number, default: 0 },
    long: { type: Number, default: 0 },
  },
  ...commonFields,
});

module.exports = mongoose.model('events', newSchema);
