const mongoose = require('mongoose');

const { Schema } = mongoose;
const commonFields = require('../common/fields');

const userSchema = new Schema({
  username: String,
  ...commonFields,
});

module.exports = mongoose.model('users', userSchema);
