const mongoose = require('mongoose');

const { Schema } = mongoose;
const commonFields = require('../common/fields');

const newSchema = new Schema({
  name: String,
  ...commonFields,
});

module.exports = mongoose.model('peers', newSchema);
