# Javascript Cebu Backend Microservice

### Stack used

The backend microservice uses a number of open source projects to work properly:

- [node.js] - evented I/O for the backend
- [Express] - fast node.js network app framework [@tjholowaychuk]
- [MongoDB] - general purpose, document-based, distributed database built for modern application developers and for the cloud era. No database is more productive to use.

### Installation

This requires [Node.js](https://nodejs.org/) v8+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd team-app-backend
$ docker-compose up -d --build
```

_NOTE:_ Ensure to change the values in the .env file before starting up the service

### Makefile

We have included a Makefile to make things much easier:

- build-image : Builds the docker image for containerization
- delete-image : Deletes the built image
- clean : Deleting the container instance and deletes the image
- run : Builds the image and starts docker-compose comtainer instances
- starts : Starts docker-compose with postgresql
- stop : Stops docker-compose created instances
- deploy : Builds and deploys the images to Google Cloud container registry

  [mongodb]: <https://www.mongodb.com/>
  [node.js]: <http://nodejs.org>
  [express]: <http://expressjs.com>

### Config

App config is a base 64 encoded value of the config yaml file. Config format looks like this:

```
mailer:
  api_key: ''
  admin_email: ''
app:
  host: '0.0.0.0'
  port: 8000
database:
  name: 'jscebu'
  username: 'root'
  password: 'root'
  host: '0.0.0.0'
  port: 27017
auth:
  domain: ''
  audience: ''
```

In order to use the config, encode it to base64 format then set it to `APP_CONFIG` environment variable.

### APIs

- Projects ( crud )
- Users ( crud )
- Events ( crud )
- Peers ( crud )
- Participants ( crud )
- Certificates ( crud )

### Third Parties

- EventBrite (events, tickets and monetization)
- Auth0 ( authentication )
- Medium ( blog and related contents )
- Disqus ( blog/comments and engagement )
