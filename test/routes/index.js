const certificates = require('./certificates');
const peers = require('./peers');
const projects = require('./projects');

module.exports = () => describe('Routes', () => {
  certificates();
  peers();
  projects();
});
