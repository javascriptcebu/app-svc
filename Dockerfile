FROM node:slim

# Create app directory
WORKDIR /usr/src/app-svc

# Bundle app source
COPY . .

#Installing Bzip for Zip Extraction
RUN apt-get update && apt-get install -y bzip2 libfontconfig

#Installing app dependencies
RUN npm install

#Exposing default port
EXPOSE 8080

ENTRYPOINT [ "npm" ]
CMD [ "start" ]

