const YAML = require('yaml');

let config = '';

// Will try to get app-config.yaml for app config but will fallback to using APP_CONFIG env
// variable, which is a base64 string format
if (process.env.APP_CONFIG) {
  const buff = Buffer.from(process.env.APP_CONFIG, 'base64');
  config = YAML.parse(buff.toString('ascii'));
} else {
  throw Error('APP_CONFIG is not set on environment. See project\'s README on how to set the config variable.');
}
const port = process.env.PORT || config.app.port || 8000;
const host = config.app.host || '0.0.0.0';

module.exports = {
  port,
  host,
  dbConfig: {
    cnx_string: config.database.cnx_string || '',
    name: config.database.name || 'jscebu',
    username: config.database.username || 'root',
    password: config.database.password || '',
    host: (dbHost = config.database.host || 'localhost'),
    port: config.database.port || 27017,
  },
  mailerConfig: {
    key: config.mailer.api_key || null,
    admin: config.mailer.admin_email || '',
  },
  auth: {
    domain: config.auth.domain || '',
    audience: config.auth.audience || '',
  },
};
