const winston = require('winston');
const expressWinston = require('express-winston');

// Formatting logs
const logformatopts = winston.format.combine(winston.format.colorize(), winston.format.simple());

const logger = expressWinston.logger({
  transports: [new winston.transports.Console()],
  format: logformatopts,
  meta: true,
  msg: 'HTTP | {{res.responseTime}}ms | {{req.method}} | {{req.url}}',
  expressFormat: true,
  colorize: true,
  /* eslint-disable-next-line */
  ignoreRoute(req, res) {
    return false;
  },
});

const errorLogger = expressWinston.errorLogger({
  transports: [new winston.transports.Console()],
  format: logformatopts,
  meta: true,
  msg: 'HTTP | {{res.responseTime}}ms | {{req.method}} | {{req.url}}',
  expressFormat: true,
  colorize: true,
});

module.exports = { logger, errorLogger };
