const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');
const jwtAuthz = require('express-jwt-authz');
const AuthenticationClient = require('auth0').AuthenticationClient;
const { auth } = require('./config');

// --- adding auth0 header ----------
// Audience should be put into secret

// Create middleware for checking the JWT

const domain = auth.domain;
const audience = auth.audience;

const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${domain}/.well-known/jwks.json`
  }),
  audience: `${audience}`,
  issuer: `https://${domain}/`,
  algorithms: ['RS256']
});

//Function getting the profile

async function getProfile(token) {
  const authClient = new AuthenticationClient({
    domain: domain
  });

  return await authClient.getProfile(token);
}

module.exports = { checkJwt, jwtAuthz, getProfile };

// ---------------------------------
