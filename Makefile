tag=latest
name=app-svc
dir = ${CURDIR}

deploy: build-image
	@docker tag ${name} registry.gitlab.com/javascriptcebu/${name}
	@docker push registry.gitlab.com/javascriptcebu/${name}

build-image: delete-image
	@docker build -t ${name}:${tag} .

delete-image:
	@docker rmi -f ${name}:${tag}

stop:
	@echo '---> stopping ${name} compose file...... <---'
	@docker-compose down

start: stop
	@echo '---> starting ${name} compose file... <---'
	@docker-compose up -d

clean: stop
	@echo '---> cleaning up docker images and containers... <---'
	@docker rmi -f `docker images | grep none | awk '{print $3}'` || true
	@docker rmi -f ${name}:${tag} || true

run: build-image
	@echo '---> running ${name} image... <---'
	@docker-compose up -d

run-db: stop-db
	@echo '---> running ${name} database image only... <---'
	@echo ${dir}
	@docker run -d --name app-database -v${dir}/volume/var/lib/mongodb/data:/var/lib/mongodb/data \
	-e MONGO_INITDB_ROOT_USERNAME=${DB_USER} -e MONGO_INITDB_ROOT_PASSWORD=${DB_PASSWORD} -p ${DB_PORT}:${DB_PORT} mongo:latest

stop-db:
	@echo '---> stopping ${name} database... <---'
	@docker rm -f `docker ps -aq --filter="name=app-database"` || true